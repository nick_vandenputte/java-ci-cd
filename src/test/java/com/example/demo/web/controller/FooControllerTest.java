package com.example.demo.web.controller;

import com.example.demo.persistence.model.Foo;
import com.example.demo.persistence.service.FooService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(FooController.class)
class FooControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private FooService service;

    @Test
    void givenNoFoo_whenFindById_thenNotFound() throws Exception {
        when(service.findById(eq(123L)))
                .thenReturn(null);

        mockMvc.perform(get("/foo/123"))
                .andExpect(status().isNotFound());
    }

    @Test
    void givenPresentFoo_whenFindById_thenFooRetrieved() throws Exception {
        final Foo foo = new Foo();
        foo.setId(123L);
        foo.setName("Darrell Swirsky");

        when(service.findById(eq(123L)))
                .thenReturn(foo);

        mockMvc.perform(get("/foo/123"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", equalTo(123)))
                .andExpect(jsonPath("$.name", equalTo("Darrell Swirsky")));
    }
}
