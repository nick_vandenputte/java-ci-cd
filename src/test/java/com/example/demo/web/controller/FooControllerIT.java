package com.example.demo.web.controller;

import com.example.demo.persistence.model.Foo;
import com.example.demo.persistence.repository.FooRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import javax.transaction.Transactional;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@Transactional
@AutoConfigureMockMvc
class FooControllerIT {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private FooRepository fooRepository;

    @Test
    void givenNoFoo_whenFindAll_thenEmptyResponse() throws Exception {
        mockMvc.perform(get("/foo"))
                .andExpect(status().isOk())
                .andExpect(content().json("[]"));
    }

    @Test
    void givenPresentFoo_whenFindAll_thenAllResponse() throws Exception {
        fooRepository.save(new Foo("Darrell Swirsky"));
        fooRepository.save(new Foo("Dave H. Bear"));

        mockMvc.perform(get("/foo"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.length()", equalTo(2)))
                .andExpect(jsonPath("$[0].name", equalTo("Darrell Swirsky")))
                .andExpect(jsonPath("$[1].name", equalTo("Dave H. Bear")));
    }
}
