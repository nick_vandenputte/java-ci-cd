package com.example.demo.persistence.service;

import com.example.demo.persistence.model.Foo;

import java.util.List;

public interface FooService {

    Foo findById(final long id);

    List<Foo> findAll();

    Foo create(final Foo foo);

    Foo update(final Foo foo);

    void deleteById(final long id);

}
