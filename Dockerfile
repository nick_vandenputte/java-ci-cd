FROM openjdk:11.0.8-jdk

VOLUME /tmp

COPY target/demo*.jar /app/app.jar

CMD [ "sh", "-c", "java $JAVA_OPTS -Dserver.port=$PORT -Xmx300m -Xss512k -XX:CICompilerCount=2 -Dfile.encoding=UTF-8 -XX:+UseContainerSupport -Djava.security.egd=file:/dev/./urandom -jar /app/app.jar" ]
